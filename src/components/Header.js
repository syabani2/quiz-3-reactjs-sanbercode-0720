import React from 'react';
import {HeaderProvider} from './HeaderContext';
import HeaderLists from './HeaderLists';

const Header = () => {
    return(
        <HeaderProvider>
            <HeaderLists></HeaderLists>
        </HeaderProvider>
    )
}

export default Header;