import React, {useContext} from 'react';
import axios from "axios";
import './Movie.css'
import {HomeContext} from './HomeContext';

const MovieForm = () => {
    const {dataContext, titleContext, descriptionContext, yearContext, durationContext, genreContext, ratingContext, idContext, formContext} = useContext(HomeContext);
    const [dataHome, setDataHome] = dataContext;
    const [inputTitle, setInputTitle] = titleContext;
    const [inputYear, setInputYear] = yearContext;
    const [inputDuration, setInputDuration] = durationContext;
    const [inputDescription, setInputDescription] = descriptionContext;
    const [inputGenre, setInputGenre] = genreContext;
    const [inputRating, setInputRating] = ratingContext;
    const [selectedId, setSelectedId] = idContext;
    const [statusForm, setStatusForm] = formContext;

    const handleSubmit = (event) => {
        event.preventDefault()

        if (inputTitle.replace(/\s/g, '') !== "") {
            if (statusForm === "create") {
                axios.post(`http://backendexample.sanbercloud.com/api/movies`, { title: inputTitle, description: inputDescription, year: inputYear, duration: inputDuration, genre: inputGenre, rating: inputRating })
                    .then(res => {
                        setDataHome([...dataHome, {id: res.data.id, title: inputTitle, description: inputDescription, year: inputYear, duration: inputDuration, genre: inputGenre, rating: inputRating }])
                    })
            } else if (statusForm === "edit") {
                axios.put(`http://backendexample.sanbercloud.com/api/movies/${selectedId}`, { title: inputTitle, description: inputDescription, year: inputYear, duration: inputDuration, genre: inputGenre, rating: inputRating })
                    .then(res => {
                        let dataHomeTemp = dataHome.find(el => el.id === selectedId)
                        dataHomeTemp.title = inputTitle
                        dataHomeTemp.description = inputDescription
                        dataHomeTemp.year = inputYear
                        dataHomeTemp.duration = inputDuration
                        dataHomeTemp.genre = inputGenre
                        dataHomeTemp.rating = inputRating
                        setDataHome([...dataHome])
                    })
            }

            setStatusForm("create")
            setSelectedId(0)
            setInputTitle("")
            setInputDescription("")
            setInputYear("")
            setInputDuration("")
            setInputGenre("")
            setInputRating("")
        }
    }

    const handleChange = (event) => {
        let typeInput = event.target.name
        switch (typeInput) {
            case "title":
                setInputTitle(event.target.value)
                break;
            case "description":
                setInputDescription(event.target.value)
                break;
            case "year":
                setInputYear(event.target.value)
                break;
            case "duration":
                setInputDuration(event.target.value)
                break;
            case "genre":
                setInputGenre(event.target.value)
                break;
            case "rating":
                setInputRating(event.target.value)
                break;
            default:
                break;
        }
    }

    return (
        <>
            <div className="cardForm">
                <h1>Form Daftar Movie</h1>
                <form onSubmit={handleSubmit}>
                    <table>
                        <tbody>
                            <tr>
                                <td>Masukan Title</td>
                                <td>: <input type="text" name="title" value={inputTitle} onChange={handleChange}></input></td>
                            </tr>
                            <tr>
                                <td>Masukan Description</td>
                                <td>: <textarea name="description" value={inputDescription} onChange={handleChange}></textarea></td>
                            </tr>
                            <tr>
                                <td>Masukan Year</td>
                                <td>: <input type="number" name="year" value={inputYear} onChange={handleChange}></input></td>
                            </tr>
                            <tr>
                                <td>Masukan Duration (menit)</td>
                                <td>: <input type="number" name="duration" value={inputDuration} onChange={handleChange}></input></td>
                            </tr>
                            <tr>
                                <td>Masukan Genre</td>
                                <td>: <input type="text" name="genre" value={inputGenre} onChange={handleChange}></input></td>
                            </tr>
                            <tr>
                                <td>Masukan Rating</td>
                                <td>: <input type="number" name="rating" value={inputRating} onChange={handleChange}></input></td>
                            </tr>
                        </tbody>
                    </table>
                    <button style={{ marginTop: "15px" }}>submit</button>
                </form>
            </div>
        </>
    )
}

export default MovieForm;
