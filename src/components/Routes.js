import React, { useContext } from "react";
import { Switch, Route } from "react-router-dom";
import Header from './Header';
import Home from './Home';
import About from './About';
import Movie from './Movie';
import Login from './LoginForm';
import { LoginContext } from "./LoginContext"

const Routes = () => {
    const [statusLogin, , , , ,] = useContext(LoginContext);

    return (
        statusLogin === 1 ? (
            <>
                <Header />
                <Switch>
                    <Route path="/about">
                        <About />
                    </Route>
                    <Route path="/movie-list">
                        <Movie />
                    </Route>
                    <Route path="/login">
                        <Login />
                    </Route>
                    <Route path="/">
                        <Home />
                    </Route>
                </Switch>
            </>
        ) : (
            <>
                <Header />
                <Switch>
                    <Route path="/about">
                        <About />
                    </Route>
                    <Route path="/login">
                        <Login />
                    </Route>
                    <Route path="/">
                        <Home />
                    </Route>
                </Switch>
            </>
        )
    );
}

export default Routes;
