import React from 'react'
import {LoginProvider} from './LoginContext'
import LoginForm from './LoginForm'
import './Home.css';

const Login = () => {
    return (
        <LoginProvider>
            <LoginForm></LoginForm>
        </LoginProvider>
    )
}

export default Login
