import React from 'react'
import './About.css'

const About = () => {
    return (
        <div className="content">
            <h1>Data Peserta Sanbercode Bootcamp ReactJs</h1>
            <p>1. <b>Nama</b> : Rizki Syaban Aryanto</p>
            <p>2. <b>Email</b> : rizkisyaban2@gmail.com</p>
            <p>3. <b>Sistem Operasi yang digunakan</b> : MacOS 10.14.6 Mojave</p>
            <p>4. <b>Akun Gitlab</b> : https://gitlab.com/syabani2</p>
            <p>5. <b>Akun Telegram</b> : @Syabany</p>
        </div>
    )
}

export default About
