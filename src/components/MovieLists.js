import React, {useContext} from 'react';
import axios from 'axios';
import {HomeContext} from './HomeContext';
import './Movie.css';

const MovieLists = () => {
    const {dataContext, titleContext, descriptionContext, yearContext, durationContext, genreContext, ratingContext, idContext, formContext} = useContext(HomeContext);
    const [dataHome, setDataHome] = dataContext;
    const [, setInputTitle] = titleContext;
    const [, setInputYear] = yearContext;
    const [, setInputDuration] = durationContext;
    const [, setInputDescription] = descriptionContext;
    const [, setInputGenre] = genreContext;
    const [, setInputRating] = ratingContext;
    const [, setSelectedId] = idContext;
    const [, setStatusForm] = formContext;

    const handleEdit = (event) => {
        let idMovie = parseInt(event.target.value)
        let movie = dataHome.find(x => x.id === idMovie)
        console.log(movie)
        setSelectedId(idMovie)
        setInputTitle(movie.title === null ? "" : movie.title)
        setInputYear(movie.year === null ? 0 : movie.year)
        setInputDescription(movie.description === null ? 0 : movie.description)
        setInputDuration(movie.duration === null ? 0 : movie.duration)
        setInputGenre(movie.genre === null ? "" : movie.genre)
        setInputRating(movie.rating === null ? 0 : movie.rating)
        setStatusForm("edit")
    }

    const handleDelete = (event) => {
        let idMovie = parseInt(event.target.value)

        let newDataHome = dataHome.filter(el => el.id !== idMovie)

        axios.delete(`http://backendexample.sanbercloud.com/api/movies/${idMovie}`)
            .then(res => {
                console.log(res)
            })

        setDataHome([...newDataHome])
    }


    return(
        <>
            <div className="contentMovieAdmin">
                <h1>Daftar Film Terbaik</h1>
                {
                    dataHome !== null && dataHome.map((val, index) => {
                        return (
                            <div className="movieAdmin" key={index}>
                                <h2>{val.title}</h2>
                                <b>
                                    <p>Rating: {val.rating}</p>
                                    <p>Durasi: {parseInt(val.duration / 60)} Jam { val.duration % 60 !== 0 ? <> {val.duration % 60} Menit </> : "" }</p>
                                    <p>Genre: {val.genre}</p>
                                    <p>Year: {val.year}</p>
                                </b>
                                <br />
                                <p><b>Description:</b> {val.description}</p>
                                <br />
                                <button onClick={handleEdit} value={val.id}>Edit</button>
                                &nbsp;
                                <button onClick={handleDelete} value={val.id}>Delete</button>
                            </div>
                        )
                    })
                }
            </div>
        </>
    )
}

export default MovieLists