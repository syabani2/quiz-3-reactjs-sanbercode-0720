import React, {useContext} from 'react';
import {HomeContext} from './HomeContext';
import './Home.css';

const HomeList = () => {
    const {dataContext} = useContext(HomeContext);
    const [dataHome] = dataContext
    console.log(dataHome)

    return(
        <>
            <div className="contentMovie">
                <h1>Daftar Film Terbaik</h1>
                {console.log(dataHome)}
                {
                    dataHome !== null && dataHome.map((val, index) => {
                        return (
                            <div className="movie" key={index}>
                                <h2>{val.title}</h2>
                                <b>
                                    <p>Rating: {val.rating}</p>
                                    <p>Durasi: {parseInt(val.duration / 60)} Jam { val.duration % 60 !== 0 ? <> {val.duration % 60} Menit </> : "" }</p>
                                    <p>Genre: {val.genre}</p>
                                    <p>Year: {val.year}</p>
                                </b>
                                <br />
                                <p><b>Description:</b> {val.description}</p>
                            </div>
                        )
                    })
                }
            </div>
        </>
    )
}

export default HomeList